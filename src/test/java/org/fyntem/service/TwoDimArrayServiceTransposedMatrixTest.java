package org.fyntem.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TwoDimArrayServiceTransposedMatrixTest {

    @Test
    public void testGetTransposedMatrix1() {
        int[][] incomeMatrix = new int[][] {{4, 5, -3}, {12, 0, 1}, {0, 9, -10}};
        int[][] transposedMatrix = TwoDimArrayService.getTransposedMatrix(incomeMatrix);

        assertArrayEquals(new int[][] {{4, 12, 0}, {5, 0, 9}, {-3, 1, -10}}, transposedMatrix);
    }

    @Test
    public void testGetTransposedMatrix2() {
        int[][] incomeMatrix = new int[][] {{4, 5}, {12, 0}, {0, 9}};
        int[][] transposedMatrix = TwoDimArrayService.getTransposedMatrix(incomeMatrix);

        assertArrayEquals(new int[][] {{4, 12, 0}, {5, 0, 9}}, transposedMatrix);
    }

    @Test
    public void testFindTransposedMatrix3() {
        int[][] incomeMatrix = new int[][] {{3, 5, 7}};
        int[][] transposedMatrix = TwoDimArrayService.getTransposedMatrix(incomeMatrix);

        assertArrayEquals(new int[][] {{3}, {5}, {7}}, transposedMatrix);
    }

    @Test
    public void testFindTransposedMatrix4() {
        int[][] incomeMatrix = new int[][] {{0}};
        int[][] transposedMatrix = TwoDimArrayService.getTransposedMatrix(incomeMatrix);

        assertArrayEquals(new int[][] {{0}}, transposedMatrix);
    }

}
