package org.fyntem.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TwoDimArrayServiceSumOfMainDiagonalTest {

    @Test
    public void testFindSumOfMainDiagonalOfSquareMatrix1() {
        int[][] incomeMatrix = new int[][] {{1, 0, 0}, {2, 3, 5}, {0, -4, -1}};
        int result = TwoDimArrayService.findSumOfMainDiagonalOfSquareMatrix(incomeMatrix);

        assertEquals(3, result);
    }

    @Test
    public void testFindSumOfMainDiagonalOfSquareMatrix2() {
        int[][] incomeMatrix = new int[][] {{1, 0, 0}, {2, -3, 5}, {0, -4, -1}, {8, 7, 0}};
        int result = TwoDimArrayService.findSumOfMainDiagonalOfSquareMatrix(incomeMatrix);

        assertEquals(-6, result);
    }

    @Test
    public void testFindSumOfMainDiagonalOfSquareMatrix3() {
        int[][] incomeMatrix = new int[][] {{3}};
        int result = TwoDimArrayService.findSumOfMainDiagonalOfSquareMatrix(incomeMatrix);

        assertEquals(3, result);
    }

    @Test
    public void testFindSumOfMainDiagonalOfSquareMatrix4() {
        int[][] incomeMatrix = new int[][] {{0}};
        int result = TwoDimArrayService.findSumOfMainDiagonalOfSquareMatrix(incomeMatrix);

        assertEquals(0, result);
    }

}
