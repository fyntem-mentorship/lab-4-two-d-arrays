package org.fyntem.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class TwoDimArrayServiceProductOfMatrixOnItsTransposedVariantTest {

    @Test
    public void testMultiplyMatrixOnItsTransposedVariant1() {
        int[][] incomeMatrix = new int[][] {{4, 5, -3}, {12, 0, 1}, {0, 9, -10}};
        int[][] resultMatrix = TwoDimArrayService.multiplyMatrixOnItsTransposedVariant(incomeMatrix);

        assertArrayEquals(new int[][] {{50, 45, 75}, {45, 145, -10}, {75, -10, 181}}, resultMatrix);
    }

    @Test
    public void testMultiplyMatrixOnItsTransposedVariant2() {
        int[][] incomeMatrix = new int[][] {{4, 5}, {12, 0}, {0, 9}};
        int[][] resultMatrix = TwoDimArrayService.multiplyMatrixOnItsTransposedVariant(incomeMatrix);

        assertArrayEquals(new int[][] {{41, 48, 45}, {48, 144, 0}, {45, 0, 81}}, resultMatrix);
    }

    @Test
    public void testMultiplyMatrixOnItsTransposedVariant3() {
        int[][] incomeMatrix = new int[][] {{3, 5, 7}};
        int[][] resultMatrix = TwoDimArrayService.multiplyMatrixOnItsTransposedVariant(incomeMatrix);

        assertArrayEquals(new int[][] {{83}}, resultMatrix);
    }

    @Test
    public void testMultiplyMatrixOnItsTransposedVariant4() {
        int[][] incomeMatrix = new int[][] {{0}};
        int[][] resultMatrix = TwoDimArrayService.multiplyMatrixOnItsTransposedVariant(incomeMatrix);

        assertArrayEquals(new int[][] {{0}}, resultMatrix);
    }

}
