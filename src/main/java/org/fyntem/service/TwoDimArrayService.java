package org.fyntem.service;

public class TwoDimArrayService {

    /**
     * Given a matrix of elements
     * A =
     * {a00, a01, a02, ... , a0k}
     * {a10, a11, a12, ... , a1k}
     * {a20, a21, a22, ... , a2k}
     * {..., ..., ..., ..., ...}
     * {an0, an1, an2, ... , ank}, {n,k} ∈ N, aij ∈ Z
     *
     * @return sum of a main diagonal (when i=j)
     */
    public static int findSumOfMainDiagonalOfSquareMatrix(int[][] incomeMatrix) {
        return 0;
    }

    /**
     * Given a matrix of elements
     * A =
     * {a00, a01, a02, ... , a0k}
     * {a10, a11, a12, ... , a1k}
     * {a20, a21, a22, ... , a2k}
     * {..., ..., ..., ..., ...}
     * {an0, an1, an2, ... , ank}, {n,k} ∈ N, aij ∈ Z
     *
     * @return transposed matrix
     */
    public static int[][] getTransposedMatrix(int[][] incomeMatrix) {
        return new int[][] {{0}};
    }

    /**
     * Given a matrix of elements
     * A =
     * {a00, a01, a02, ... , a0k}
     * {a10, a11, a12, ... , a1k}
     * {a20, a21, a22, ... , a2k}
     * {..., ..., ..., ..., ...}
     * {an0, an1, an2, ... , ank}, {n,k} ∈ N, aij ∈ Z
     *
     * and a String with element number
     *
     * @return product of income matrix on it's transposed variant
     */
    public static int[][] multiplyMatrixOnItsTransposedVariant(int[][] incomeMatrix) {
        return new int[][] {{0}};
    }

}
